# MERN CRUD application

This is an application that will initially use the **MERN** stack to perform simple **CRUD** (**C**reate, **R**ead, **U**pdate, **D**elete) database commands.
The goal of this project is to understand how to create forms and store them in databases, as this will be keep to complete my final apprenticeship project.
I have used MongoDB, a `NoSQL` database, initially as the final project does not require any relational querying.

## MERN

- **M**ongoDB: data storage
- **E**xpress: web framework, back-end 
- **R**eact: renders views, front-end
- **N**ode: runtime environment

Similar stacks include MEAN (Angular) and PERN (Postgres).

## To run the project
### Using `docker-compose`

The `docker-compose` file can be used to quickly start this project.
It uses a Dockerised MongoDB instance and creates a `volume` to persist the data when containers are stopped.

1. Run `docker-compose build`
2. Run `docker-compose up -d` to build the application
3. The application will be available at `http://localhost:5000`
4. Stop the application with `docker-compose down`

### Using an env file

The application can also use a MongoDB Atlas cluster to persist data.
To do this, you will need to [create a cluster](https://docs.atlas.mongodb.com/getting-started/?_ga=2.72803035.990843533.1657875862-2107331736.1652871876).
Once you have created a cluster and retrieved the connection string, follow the next steps.

1. Create a file at `server/config.env` with the following parameters:
- `ATLAS_URI`: the URI of your MongoDB Atlas cluster, e.g. `mongodb+srv://<username>:<password>@<cluster-id>`
- `PORT`: port for the application to run on
2. Uncomment line 5 in `/server/server.js`
3. Run `npm install` in `/server`
4. Run `npm start` in `/server`
5. In a new terminal, run `npm install` in `/client`
6. Run `npm start` in `/client`
7. This will start the application at `http://localhost:5000`

I will look at improving this process, so it also uses `docker-compose`.

## Resources used

- [MongoDB MERN tutorial](https://www.mongodb.com/languages/mern-stack-tutorial)
