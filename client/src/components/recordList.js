// fetches all the records as a list

import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

// this is a class for the record
const Record = (props) => (
    <tr>
      <td>{props.record.name}</td>
      <td>{props.record.position}</td>
      <td>{props.record.level}</td>
      <td>
        <Link className="btn btn-link" to={`/edit/${props.record._id}`}>Edit</Link> |
        <button className="btn btn-link"
                onClick={() => {
                  props.deleteRecord(props.record._id);
                }}
        >
          Delete
        </button>
      </td>
    </tr>
);

export default function RecordList() {
  // no starting state required
  const [records, setRecords] = useState([]);

  // get all records from database
  useEffect(() => {
    async function getRecords() {
      const response = await fetch(`http://localhost:5000/record/`);

      if (!response.ok) {
        const message = `An error occurred: ${response.statusText}`;
        window.alert(message);
        return;
      }

      const records = await response.json();
      // sets the records in the state
      setRecords(records);
    }

    getRecords();

    return;
    // depends on there being records
  }, [records.length]);

  // deletes a record when it has been clicked
  async function deleteRecord(id) {
    await fetch(`http://localhost:5000/${id}`, {
      method: "DELETE"
    });

    const newRecords = records.filter((el) => el._id !== id);
    setRecords(newRecords);
  }

  // maps record on the table
  function recordList() {
    return records.map((record) => {
      return (
          <Record
              record={record}
              deleteRecord={() => deleteRecord(record._id)}
              key={record._id}
          />
      );
    });
  }

  // displays the table with the records of individuals.
  return (
      <div>
        <h3>Record List</h3>
        <table className="table table-striped" style={{ marginTop: 20 }}>
          <thead>
          <tr>
            <th>Name</th>
            <th>Position</th>
            <th>Level</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>{recordList()}</tbody>
        </table>
      </div>
  );
}
