import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import { BrowserRouter } from "react-router-dom";

ReactDOM.render(
    // BrowserRouter keeps UI in sync with the URL
    // It makes the website responsive by only reloading this part of the page
    // rather than the entire page
    <React.StrictMode>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </React.StrictMode>,
    document.getElementById("root")
)
