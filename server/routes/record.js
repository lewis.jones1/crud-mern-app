const express = require("express");

// router controls requests starting with path /record.
const recordRoutes = express.Router();

// handles database connection
const dbo = require("../db/conn");

// converts id from request string to ObjectId for the _id.
const ObjectId = require("mongodb").ObjectId;

// gets all records
recordRoutes.route("/record").get(function (req, res) {
  let dbConnect = dbo.getDb();
  dbConnect
  .collection("records")
  .find({})
  .toArray(function (err, result) {
    if (err) throw err
    res.json(result);
  });
});

// gets a single record by id
recordRoutes.route("/record/:id").get(function (req, res) {
  let dbConnect = dbo.getDb();
  let myQuery = { _id: ObjectId( req.params.id )};
  dbConnect
  .collection("records")
  .findOne(myQuery, function (err, result) {
    if (err) throw err;
    res.json(result);
  });
});

// creates a new record
recordRoutes.route("/record/add").post(function (req, response) {
  let dbConnect = dbo.getDb();
  let requestObject = {
    name: req.body.name,
    position: req.body.position,
    level: req.body.level,
  };
  dbConnect.collection("records").insertOne(requestObject, function (err, res) {
    if (err) throw err;
    console.log("1 document added");
    response.json(res);
  });
});

// This section will help you update a record by id.
recordRoutes.route("/update/:id").post(function (req, response) {
  let dbConnect = dbo.getDb();
  let myQuery = { _id: ObjectId( req.params.id )};
  let newvalues = {
    $set: {
      name: req.body.name,
      position: req.body.position,
      level: req.body.level,
    },
  };
  dbConnect
  .collection("records")
  .updateOne(myQuery, newvalues, function (err, res) {
    if (err) throw err;
    console.log("1 document updated");
    response.json(res);
  });
});

// This section will help you delete a record
recordRoutes.route("/:id").delete((req, response) => {
  let dbConnect = dbo.getDb();
  let myQuery = { _id: ObjectId( req.params.id )};
  dbConnect.collection("records").deleteOne(myQuery, function (err, obj) {
    if (err) throw err;
    console.log("1 document deleted");
    response.json(obj);
  });
});

module.exports = recordRoutes;
